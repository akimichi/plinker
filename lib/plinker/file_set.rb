module Plinker
  module FileSet
    require 'pathname'

    class Base
      attr_accessor :root_path
      require 'test/unit'
      include Test::Unit::Assertions
      
      def initialize(path) 
        @root_path = Pathname(path)
        assert(@root_path.dirname.directory?)
      end
    end
    
    class TextFileSet < Base
      attr_accessor :ped_filepath, :map_filepath

      def initialize(path)
        super(path)
        @ped_filepath = @root_path.dirname + "#{@root_path.basename}.ped"
        @map_filepath = @root_path.dirname + "#{@root_path.basename}.map"
      end

      def exist?
        @ped_filepath.exist? && @map_filepath.exist?
      end
    end
    
    class BinaryFileSet < Base
      attr_accessor :bed_filepath, :bim_filepath, :fam_filepath

      def initialize(path)
        super(path)
        @bed_filepath = @root_path.dirname + "#{@root_path.basename}.bed"
        @bim_filepath = @root_path.dirname + "#{@root_path.basename}.bim"
        @fam_filepath = @root_path.dirname + "#{@root_path.basename}.fam"
      end

      def exist?
        @bed_filepath.exist? && @bim_filepath.exist? && @fam_filepath.exist?
      end
    end

    class MissingFileSet < Base
      attr_accessor :imiss_filepath, :lmiss_filepath

      def initialize(path)
        super(path)
        @imiss_filepath = @root_path.dirname + "#{@root_path.basename}.imiss"
        @lmiss_filepath = @root_path.dirname + "#{@root_path.basename}.lmiss"
      end
    end

    class FreqFileSet < Base
      attr_accessor :freq_filepath

      def initialize(path)
        super(path)
        @freq_filepath = @root_path.dirname + "#{@root_path.basename}.frq"
      end
    end

    class HardyFileSet < Base
      attr_accessor :hardy_filepath

      def initialize(path)
        super(path)
        @hardy_filepath = @root_path.dirname + "#{@root_path.basename}.hwe"
      end
    end

    class AssocFileSet < Base
      attr_accessor :assoc_filepath

      def initialize(path)
        super(path)
        @assoc_filepath = @root_path.dirname + "#{@root_path.basename}.assoc"
      end
    end

    class IndepFileSet < Base
      attr_accessor :prune_in_filepath, :prune_out_filepath

      def initialize(path)
        super(path)
        @prune_in_filepath = @root_path.dirname + "#{@root_path.basename}.prune.in"
        @prune_out_filepath = @root_path.dirname + "#{@root_path.basename}.prune.out"
      end
    end

    class GenomeFileSet < Base
      attr_accessor :genome_filepath

      def initialize(path)
        super(path)
        @genome_filepath = @root_path.dirname + "#{@root_path.basename}.genome"
      end
    end
  end
end


