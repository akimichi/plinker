# -*- coding: utf-8 -*-
module Plinker
  module Command
    require 'open3'
    require 'test/unit'

    class Base
      attr_accessor :command

      def execute
        stdout, stderr, status = Open3.capture3(@command)
        if status.exitstatus == 0
          puts stdout
        else
          STDERR.puts "There was a problem running '#{@command}'"
          STDERR.puts stderr
          exit -1
        end
      end

      def file_arg(input_path)
        case input_path 
        when FileSet::TextFileSet
          "--file #{input_path.root_path}"
        when FileSet::BinaryFileSet
          "--bfile #{input_path.root_path}"
        else
          raise
        end
      end
    end

    class Check < Base
      include Test::Unit::Assertions

      def initialize(input_path, options = {})
        args = []
        options.each{|key,value|
          case key 
          when :extract
            args << "--extract #{value}"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} #{args.join(' ')}"
      end
    end

    class MakeBed < Base
      include Test::Unit::Assertions

      # options:
      #   --mind
      #   --geno
      #   --maf
      #   --extract
      #   --exclude
      #   --keep
      #   --remove 
      def initialize(input_path, output_path, options = {})
        args = Array.new
        options.each{|key,value|
          case key 
          when :mind
            args.push "--mind #{value}"
          when :geno
            args.push "--geno #{value}"
          when :maf
            args.push "--maf #{value}"
          when :hwe
            args.push "--hwe #{value}"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} --make-bed #{args.join(' ')} --out #{output_path.root_path}"
      end
    end

    class Assoc < Base
      include Test::Unit::Assertions
      # options:
      #   --adjust
      def initialize(input_path, output_path, options = {})
        args = Array.new
        options.each{|key,value|
          case key 
          when :adjust
            args.push "--adjust"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} --assoc #{args.join(' ')} --out #{output_path.root_path}"
      end
    end

    class Mh < Base
      include Test::Unit::Assertions
      # options:
      #   --adjust
      def initialize(input_path, output_path, options = {})
        args = Array.new
        options.each{|key,value|
          case key 
          when :adjust
            args.push "--adjust"
          when :within
            args.push "--within #{value}"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} --mh #{args.join(' ')} --out #{output_path.root_path}"
      end
    end

    #
    # --indep
    #   which prunes based on the variance inflation factor (VIF), which recursively removes SNPs 
    #   within a sliding window
    #

    # --indep-pairwise
    #   which is similar, except it is based only on pairwise genotypic correlation.
    #
    # The output of either of these commands is two lists of SNPs: 
    # those that are pruned out and those that are not. 
    # A separate command using the --extract or --exclude option is necessary to actually perform the pruning.
    # 
    class IndepPairwise < Base
      include Test::Unit::Assertions
      # options:
      #   --
      #   The parameters for --indep are: 
      #      1) window size in SNPs (e.g. 50), 
      #      2) the number of SNPs to shift the window at each step (e.g. 5), 
      #      3) the VIF threshold.

      def initialize(input_path, output_path, window_size, number_of_SNPs, vif_threshhold)
        assert(window_size.is_a?(Integer))
        assert(number_of_SNPs.is_a?(Integer))
        assert(vif_threshhold.is_a?(Numeric))
        @command = "p-link #{file_arg(input_path)} --indep-pairwise #{window_size} #{number_of_SNPs} #{vif_threshhold} --out #{output_path.root_path}"
      end
    end

    # --record
    #  convert my binary PED fileset back into a standard PED/MAP fileset
    #  c.f. http://pngu.mgh.harvard.edu/~purcell/plink/dataman.shtml#recode

    class Recode < Base
      include Test::Unit::Assertions
      # options:
      #   --snp : snp identifier

      def initialize(input_path, output_path, options = {})
        args = []
        options.each{|key,value|
          case key 
          when :snp
            args << "--snp #{value}"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} --recode #{args.join(' ')} --out #{output_path.root_path}"
      end
    end

    class Missing < Base
      include Test::Unit::Assertions
      # options:
      #   --all
      #   --chr : Integer

      def initialize(input_path, options = {})
        args = []
        options.each{|key,value|
          case key 
          when :chr
            args << "--extract #{value}"
          when :all
            args << "--all"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} --missing #{args.join(' ')}"
      end
    end

    class Genome < Base
      include Test::Unit::Assertions
      # options:
      #   --extract

      def initialize(input_path, output_path, options = {})
        args = []
        options.each{|key,value|
          case key 
          when :extract
            args << "--extract #{value}"
          else
            raise
          end
        }
        @command = "p-link #{file_arg(input_path)} --genome #{args.join(' ')} --out #{output_path.root_path}"
      end
    end
  end
end
