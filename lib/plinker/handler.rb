# -*- coding: utf-8 -*-
module Plinker
  module Handler
    require 'open3'
    require 'test/unit'
    include Test::Unit::Assertions

    require 'plinker/command.rb'
    include Plinker::Command

    private
    def execute(command, output)
      stdout, stderr, status = Open3.capture3(command)
      if status.exitstatus == 0
        puts stdout
        output
      else
        STDERR.puts "There was a problem running '#{command}'"
        STDERR.puts stderr
        exit -1
      end
    end

    public
    # Just typing plink and specifying a file with no further options is a good way to check that the file is
    # intact, and to get some basic summary statistics about the file.
    #   plink --file hapmap1
    # The --file option takes a single parameter, the root of the input file names, and will look for two files:
    # a PED file and a MAP file with this root name. In otherwords, --file hapmap1 implies hapmap1.ped and
    # hapmap1.map should exist in the current directory.
    def check(input_path)
      Check.new(input_path).execute
      input_path
    end

    def make_bed(input_path, output_path, options = {})
      assert(input_path.instance_of?(FileSet::TextFileSet) || input_path.instance_of?(FileSet::BinaryFileSet))
      assert(input_path.exist?)
      assert(output_path.instance_of? FileSet::BinaryFileSet)

      MakeBed.new(input_path, output_path, options).execute
      output_path
    end

    # Summary statistics: missing rates
    # Next, we shall generate some simple summary statistics on rates of missing data in the file, using the
    # --missing option:
    #   plink --bfile hapmap1 --missing --out miss stat
    
    # which should generate the following output:
    #   ...
    #   0 of 89 individuals removed for low genotyping ( MIND > 0.1 )
    #   Writing individual missingness information to [ miss stat.imiss ]
    #   Writing locus missingness information to [ miss stat.lmiss ]
    #   ...
    def missing(binary_input_path, missing_output_path)
      assert(binary_input_path.instance_of? FileSet::BinaryFileSet)
      assert(missing_output_path.instance_of? FileSet::MissingFileSet)
      command = "p-link --bfile #{binary_input_path.root_path} --missing --out #{missing_output_path.root_path}"
      execute(command, missing_output_path)
    end

    # Summary statistics: allele frequencies
    # Next we perform a similar analysis, except requesting allele frequencies instead of genotyping rates. The
    # following command generates a file called freq stat.frq which contains the minor allele frequency and
    # allele codes for each SNP.
    #    plink --bfile hapmap1 --freq --out freq stat
    def freq(binary_input_path, freq_output_path)
      assert(binary_input_path.instance_of? FileSet::BinaryFileSet)
      assert(freq_output_path.instance_of? FileSet::FreqFileSet)
      command = "p-link --bfile #{binary_input_path.root_path} --freq --out #{freq_output_path.root_path}"
      execute(command, freq_output_path)
    end
    
    # To generate a list of genotype counts and Hardy-Weinberg test statistics for each SNP, use the option:
    #   plink --file data --hardy
    # which creates a file:
    #   plink.hwe
    def hardy(binary_input_path, hardy_output_path)
      assert(binary_input_path.instance_of? FileSet::BinaryFileSet)
      assert(hardy_output_path.instance_of? FileSet::HardyFileSet)
      command = "p-link --bfile #{binary_input_path.root_path} --hardy --out #{hardy_output_path.root_path}"
      execute(command, hardy_output_path)
    end

    # Basic association analysis
    # Let’s now perform a basic association analysis on the disease trait for all single SNPs. The basic command is
    #    plink --bfile hapmap1 --assoc --out as1
    # which generates an output file as1.assoc which contains the following fields
    def assoc(input_path, output_path, options = {})
      assert(input_path.instance_of? FileSet::BinaryFileSet)
      assert(output_path.instance_of? FileSet::AssocFileSet)
      # command = "p-link --bfile #{binary_input_path.root_path} --assoc --out #{assoc_output_path.root_path}"
      # execute(command, assoc_output_path)
      Assoc.new(input_path, output_path, options).execute
      output_path
    end

    # Sometimes it is useful to generate a pruned subset of SNPs that are in approximate linkage equilibrium with
    # each other. This can be achieved via two commands: --indep which prunes based on the variance inflation
    # factor (VIF), which recursively removes SNPs within a sliding window
    # The VIF pruning routine is performed:
    #   plink --file data --indep 50 5 2
    # will create files
    #   plink.prune.in
    #   plink.prune.out
    def indep_pairwise(binary_input_path, indep_output_path)
      assert(binary_input_path.instance_of? FileSet::BinaryFileSet)
      assert(indep_output_path.instance_of? FileSet::IndepFileSet)
      command = "p-link --bfile #{binary_input_path.root_path} --indep --out #{indep_output_path.root_path}"
      execute(command, indep_output_path)
    end

    # In a homogeneous sample, it is possible to calculate genome-wide IBD given IBS information, as long
    # as a large number of SNPs are available (probably 1000 independent SNPs at a bare minimum; ideally 100K or more).
    #   plink --file mydata --genome
    # which creates the file
    #   plink.genome
    def genome(binary_input_path, genome_output_path)
      assert(binary_input_path.instance_of? FileSet::BinaryFileSet)
      assert(genome_output_path.instance_of? FileSet::GenomeFileSet)
      command = "p-link --bfile #{binary_input_path.root_path} --genome --out #{genome_output_path.root_path}"
      execute(command, genome_output_path)
    end
    
  end
end
