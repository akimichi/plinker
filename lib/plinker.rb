require 'plinker/version.rb'

# Add requires for other files you add to your project here, so
# you just need to require this one file in your bin file

require 'plinker/handler.rb'
require 'plinker/file_set.rb'
require 'plinker/command.rb'
