# -*- coding: utf-8 -*-

require "plinker"

describe Plinker::Handler do
  include Plinker::Handler

  describe "using wgas1 in http://pngu.mgh.harvard.edu/~purcell/plink/res.shtml" do
    before do
      @root_path = 'spec/wgas1'
      @output_root_path = 'spec/output/wgas1'
    end
    
    it "check #{@root_path}" do 
      text_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      expect(check(text_file_set)).to eq(text_file_set)
    end

    it "plink --file wgas1 --make-bed --out wgas2" do 
      input_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      output_file_set = Plinker::FileSet::BinaryFileSet.new('spec/output/wgas2')
      expect(make_bed(input_file_set, output_file_set)).to eq(output_file_set)
    end

    it "plink --bfile wgas2" do 
      input_file_set = Plinker::FileSet::BinaryFileSet.new('spec/output/wgas2')
      expect(check(input_file_set)).to eq(input_file_set)
    end

    it "plink --bfile wgas2 --freq --out freq1" do 
      input_file_set = Plinker::FileSet::BinaryFileSet.new('spec/output/wgas2')
      output_file_set = Plinker::FileSet::FreqFileSet.new('spec/output/freq1')
      expect(freq(input_file_set, output_file_set)).to eq(output_file_set)
    end
    
    it "plink --bfile wgas2 --hardy --out hwe1" do 
      input_file_set = Plinker::FileSet::BinaryFileSet.new('spec/output/wgas2')
      output_file_set = Plinker::FileSet::HardyFileSet.new('spec/output/hwe1')
      expect(hardy(input_file_set, output_file_set)).to eq(output_file_set)
    end

    it "plink --bfile wgas2 --maf 0.01 --geno 0.05 --mind 0.05 --hwe 1e-3 --make-bed --out wgas3" do
      input_file_set = Plinker::FileSet::BinaryFileSet.new('spec/output/wgas2')
      output_file_set = Plinker::FileSet::BinaryFileSet.new('spec/output/wgas3')
      expect(make_bed(input_file_set, output_file_set, {:maf => 0.01, :geno => 0.05, :mind =>  0.05, :hwe => 1e-3})).to eq(output_file_set)
    end
  end

  describe "using c_2150SNPs" do
    before do
      @root_path = 'spec/c_2150SNPs'
    end

    it "check #{@root_path}" do 
      text_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      expect(check(text_file_set)).to eq(text_file_set)
    end

    # it "make_bed will fail" do 
    #   text_file_set = Plinker::FileSet::TextFileSet.new('spec/sample')
    #   binary_file_set = Plinker::FileSet::BinaryFileSet.new('spec/sample')
    #   # make_bed(text_file_set, binary_file_set).to raise_exception(MiniTest::Assertion)
    #   begin
    #     make_bed(text_file_set, binary_file_set)
    #   rescue SystemExit => e
    #     expect(e.status).to eq(-1)
    #   end
    # end

    it "make_bed from root_path" do 
      text_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      binary_file_set = Plinker::FileSet::BinaryFileSet.new(@root_path)
      expect(make_bed(text_file_set, binary_file_set)).to eq(binary_file_set)
    end

    it "perform missing from root_path" do 
      binary_file_set = Plinker::FileSet::BinaryFileSet.new(@root_path)
      missing_file_set = Plinker::FileSet::MissingFileSet.new(@root_path)
      expect(missing(binary_file_set, missing_file_set)).to eq(missing_file_set)
    end

    it "perform freq from root_path" do 
      binary_file_set = Plinker::FileSet::BinaryFileSet.new(@root_path)
      freq_file_set = Plinker::FileSet::FreqFileSet.new(@root_path)
      expect(freq(binary_file_set, freq_file_set)).to eq(freq_file_set)
    end

    it "perform assoc from root_path" do 
      binary_file_set = Plinker::FileSet::BinaryFileSet.new(@root_path)
      assoc_file_set = Plinker::FileSet::AssocFileSet.new(@root_path)
      expect(assoc(binary_file_set, assoc_file_set)).to eq(assoc_file_set)
    end
  end
end
