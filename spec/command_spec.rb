# -*- coding: utf-8 -*-

require "plinker"

describe Plinker::Command do
  include Plinker::Command

  before do
    @root_path = 'spec/wgas1'
    @output_root_path = 'spec/output/wgas1'
  end
  
  
  it Plinker::Command::Check do 
    text_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
    check = Plinker::Command::Check.new(text_file_set)
    expect(check.command).to eq("p-link --file spec/wgas1 ")
  end

  describe Plinker::Command::MakeBed do 
    it "default options" do
      input_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      output_file_set = Plinker::FileSet::BinaryFileSet.new(@output_root_path)
      make_bed = Plinker::Command::MakeBed.new(input_file_set, output_file_set)
      expect(make_bed.command).to eq("p-link --file #{@root_path} --make-bed  --out #{@output_root_path}")
    end
    it "one option as maf" do
      input_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      output_file_set = Plinker::FileSet::BinaryFileSet.new(@output_root_path)
      make_bed = Plinker::Command::MakeBed.new(input_file_set, output_file_set, {:maf => 0.05})
      expect(make_bed.command).to eq("p-link --file #{@root_path} --make-bed --maf 0.05 --out #{@output_root_path}")
    end
    it "two option as maf and geno" do
      input_file_set = Plinker::FileSet::TextFileSet.new(@root_path)
      output_file_set = Plinker::FileSet::BinaryFileSet.new(@output_root_path)
      make_bed = Plinker::Command::MakeBed.new(input_file_set, output_file_set, {:maf => 0.05, :geno => 0.01})
      expect(make_bed.command).to eq("p-link --file #{@root_path} --make-bed --maf 0.05 --geno 0.01 --out #{@output_root_path}")
    end
  end
  describe Plinker::Command::Assoc do 
    it "plink --bfile wgas3 --assoc --adjust --out assoc1" do
      input_file_set = Plinker::FileSet::TextFileSet.new('wgas3')
      output_file_set = Plinker::FileSet::BinaryFileSet.new('assoc1')
      assoc = Plinker::Command::Assoc.new(input_file_set, output_file_set, {:adjust => true})
      expect(assoc.command).to eq("p-link --file wgas3 --assoc --adjust --out assoc1")
    end
  end

  describe Plinker::Command::Mh do 
    it "plink --bfile wgas3 --mh --within pop.cov --adjust --out cmh1" do
      input_file_set = Plinker::FileSet::TextFileSet.new('wgas3')
      output_file_set = Plinker::FileSet::BinaryFileSet.new('cmh1')
      mh = Plinker::Command::Mh.new(input_file_set, output_file_set, {:within => 'pop.cov', :adjust => true})
      expect(mh.command).to eq("p-link --file wgas3 --mh --within pop.cov --adjust --out cmh1")
    end
  end

  describe Plinker::Command::IndepPairwise do 
    it "plink --bfile wgas3 --indep-pairwise 50 10 0.2 --out prune1" do
      input_file_set = Plinker::FileSet::TextFileSet.new('wgas3')
      output_file_set = Plinker::FileSet::BinaryFileSet.new('prune1')
      indep_pairwise = Plinker::Command::IndepPairwise.new(input_file_set, output_file_set, 50, 10, 0.2)
      expect(indep_pairwise.command).to eq("p-link --file wgas3 --indep-pairwise 50 10 0.2 --out prune1")
    end
  end


  describe Plinker::Command::Missing do 
    it "plink --file tophit --all --missing" do
      input_file_set = Plinker::FileSet::TextFileSet.new('tophit')
      missing = Plinker::Command::Missing.new(input_file_set, {:all => true})
      expect(missing.command).to eq("p-link --file tophit --missing --all")
    end
  end

  describe Plinker::Command::Recode do
    it "plink --bfile wgas3 --recode --snp rs11204005 --out tophit" do
      input_file_set = Plinker::FileSet::BinaryFileSet.new('wgas3')
      output_file_set = Plinker::FileSet::BinaryFileSet.new('tophit')
      recode = Plinker::Command::Recode.new(input_file_set, output_file_set, {:snp => "rs11204005"})
      expect(recode.command).to eq("p-link --bfile wgas3 --recode --snp rs11204005 --out tophit")
    end
  end

  describe Plinker::Command::Genome do 
    it "plink --bfile wgas3 --extract prune1.prune.in --genome --out ibs1" do
      input_file_set = Plinker::FileSet::TextFileSet.new('wgas3')
      output_file_set = Plinker::FileSet::BinaryFileSet.new('prune1')
      genome = Plinker::Command::Genome.new(input_file_set, output_file_set, {:extract => "prune1.prune.in"})
      expect(genome.command).to eq("p-link --file wgas3 --genome --extract prune1.prune.in --out prune1")
    end
  end
end
