# -*- coding: utf-8 -*-

require "plinker"

describe Plinker::FileSet do
  include Plinker::FileSet

  describe "TextFileSet with sample" do
    text_file_set = Plinker::FileSet::TextFileSet.new('spec/sample')
    it "should check file path" do 
      expect(text_file_set.ped_filepath.to_s).to eq('spec/sample.ped')
      expect(text_file_set.map_filepath.to_s).to eq('spec/sample.map')
    end
    it "should exist? " do 
      expect(text_file_set.exist?).to eq(false)
    end
  end

  describe "TextFileSet with actual data" do
    text_file_set = Plinker::FileSet::TextFileSet.new('spec/c_2150SNPs')
    it "should check file path" do 
      expect(text_file_set.ped_filepath.to_s).to eq('spec/c_2150SNPs.ped')
      expect(text_file_set.map_filepath.to_s).to eq('spec/c_2150SNPs.map')
    end
    it "should exist? " do 
      expect(text_file_set.exist?).to eq(true)
    end
  end

  describe "BinaryFileSet" do
    it "should create BinaryFileSet" do 
      text_file_set = Plinker::FileSet::BinaryFileSet.new('spec/sample')
      expect(text_file_set.bed_filepath.to_s).to eq('spec/sample.bed')
      expect(text_file_set.bim_filepath.to_s).to eq('spec/sample.bim')
      expect(text_file_set.fam_filepath.to_s).to eq('spec/sample.fam')
    end
  end
end
